<?php
/**
 * Template Name: Quem Somos
 * Description: Quem Somos
 *
 * @package Unika
 */

get_header(); ?>
<!-- PÁGINA QUEM SOMOS -->
<div class="pg pg-quemSomos">
	<div class="topoPag"></div>
	
	<figure class="banner" style="background: url(<?php echo $configuracao['quem_somos_banner_foto']['url'] ?>);">
		<div class="container">
			<p><?php echo $configuracao['quem_somos_banner_titulo'] ?></p>

			<img src="<?php echo $configuracao['quem_somos_banner_logo']['url'] ?>" alt="Quem Somos">
		</div>
	</figure>

	<div class="tituloPg" id="quemsomos">
		<h6><?php echo $configuracao['quem_somos_integrantes_titulo'] ?></h6>
		<p><?php echo $configuracao['quem_somos_integrantes_texto'] ?></p>
	</div>

	<section class="sessaointegrantes">
		<?php 
			$contador = 0;
			//LOOP DE POST DESTAQUES
			$posEquipe = new WP_Query( array( 'post_type' => 'equipe', 'orderby' => 'id', 'order' => 'desc', 'posts_per_page' => -1) );
			
			while ( $posEquipe->have_posts() ) : $posEquipe->the_post();
				$fotoEquipe = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
				$fotoEquipe = $fotoEquipe[0];
				$equipe_facebook = rwmb_meta('Unika_equipe_facebook');
				$equipe_linkedin = rwmb_meta('Unika_equipe_linkedin');
				$equipe_instagram = rwmb_meta('Unika_equipe_instagram');
				
			if ($contador % 2 == 0):
				
			
		 ?>
			<div class="integrante">
				<div class="container">
					<div class="row">
						<div class="col-sm-6">
							<div class="perfil">
								<img src="<?php echo $fotoEquipe ?> " alt="" class="">
							</div>
						</div>
						<div class="col-sm-6">
							<div class="perfil">
								<h2><?php echo get_the_title() ?></h2>	
								<p><?php echo get_the_content() ?></p>
								<div class="redesSociais">
									<?php 	if ($equipe_facebook):?>
										<a href=" <?php echo $equipe_facebook ?> " target="_blank" title="Facebook">
											<i class="fa fa-facebook" aria-hidden="true"></i>
										</a>
										<?php endif; ?>

									<?php 	if ($equipe_linkedin):?>
									<a href=" <?php echo $equipe_linkedin ?> " target="_blank" title="Linkedin">
										<i class="fa fa-linkedin" aria-hidden="true"></i>
									</a>
									<?php endif; ?>
									<?php 	if ($equipe_instagram):?>
									<a href=" <?php echo $equipe_instagram ?> " target="_blank" title="Instagram">
										<i class="fa fa-instagram" aria-hidden="true"></i>
									</a>
									<?php endif; ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php else: ?>
			<div class="integrante">
				<div class="container">
					<div class="row">
						
						<div class="col-sm-6">
							<div class="perfil">
								<h2><?php echo get_the_title() ?></h2>	
								<p><?php echo get_the_content() ?></p>
								<div class="redesSociais">
									<?php 	if ($equipe_facebook):?>
										<a href=" <?php echo $equipe_facebook ?> " target="_blank" title="Facebook">
											<i class="fa fa-facebook" aria-hidden="true"></i>
										</a>
										<?php endif; ?>

									<?php 	if ($equipe_linkedin):?>
									<a href=" <?php echo $equipe_linkedin ?> " target="_blank" title="Linkedin">
										<i class="fa fa-linkedin" aria-hidden="true"></i>
									</a>
									<?php endif; ?>
									<?php 	if ($equipe_instagram):?>
									<a href=" <?php echo $equipe_instagram ?> " target="_blank" title="Instagram">
										<i class="fa fa-instagram" aria-hidden="true"></i>
									</a>
									<?php endif; ?>
								</div>
							</div>
						</div>

						<div class="col-sm-6">
							<div class="perfil">
								<img src="<?php echo $fotoEquipe ?>" alt="" class="">
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php endif; ?>
	<?php $contador++;endwhile; wp_reset_query(); ?>
	
	</section>

	<!-- SESSÃO ONDE ATUAMOS -->
	<section class="sessaoOndeAtuamos background">
		
		<div class="container correcaoPX">
			<div class="row">
				<div class="col-md-6">
					<div class="titulo">
						<h6><?php echo $configuracao['pg_inicial_onde_titulo'] ?></h6>
						<p><?php echo $configuracao['pg_inicial_onde_texto'] ?></p>

						<a href="<?php echo home_url('/contato/'); ?>">Entre em contato</a>
					</div>
				</div>
				<div class="col-md-6">
					<div class="listaPaises" style="display: none;">
						<ul>
							<?php 
								//LOOP DE POST DESTAQUES
								$posPaises = new WP_Query( array( 'post_type' => 'paises', 'orderby' => 'id', 'order' => 'desc', 'posts_per_page' => -1) );
								while ( $posPaises->have_posts() ) : $posPaises->the_post();
							 ?>
							<li>
								<span><?php echo get_the_title() ?></span>
							</li>
							<?php endwhile; wp_reset_query(); ?>
						</ul>
					</div>
				</div>
			</div>

			<style>	
				.popUp-News #btnCadastrarNews{
					border: none!important;
				}
		</style>

			<div class="paises">
				<ul>
					<?php 
						//LOOP DE POST DESTAQUES
						$posPaises = new WP_Query( array( 'post_type' => 'paises', 'orderby' => 'id', 'order' => 'desc', 'posts_per_page' => -1) );
						while ( $posPaises->have_posts() ) : $posPaises->the_post();
						$fotoPaises = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
						$fotoPaises = $fotoPaises[0];
						
					?>
					<li>
						<img src="<?php echo $fotoPaises  ?>" alt="<?php echo get_the_title() ?>">
						<h2><?php echo get_the_title() ?></h2>
						<p><?php echo get_the_content() ?></p>
					</li>
					<?php endwhile; wp_reset_query(); ?>
				</ul>
			</div>
		</div>

	</section>

	<!-- SESSÃO DE SERVIÇOS -->
	<section class="listasessaoServicos background">
		<h6 id="problemas-que-resolvemos"><?php echo $configuracao['pg_inicial_problemas_titulo'] ?></h6>
			<p><?php echo $configuracao['pg_inicial_problemas_texto'] ?></p>
		<div class="container">
			
			<ul>
				<?php 
					//LOOP DE POST SERVIÇOS
					$posServicos = new WP_Query( array( 'post_type' => 'servico', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1) );
					while ( $posServicos->have_posts() ) : $posServicos->the_post();
					$fotoServicos = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
					$fotoServicos = $fotoServicos[0];
					
				 ?>
				<!-- SERVIÇOS -->
				<li>
					<div class="servico">
						<img src="<?php echo $fotoServicos ?>" alt="<?php echo get_the_title() ?>">
						<p><?php echo get_the_content() ?></p>
					</div>
				</li>
				<?php endwhile; wp_reset_query(); ?>

			</ul>

		</div>

	</section>

	<!-- SESSÃO  LOGOS PARCEIROS -->
	<section class="sessaoParceiros background">
		<h6 class="hidden">Parceiros</h6>
		<!-- CARROSSEL DE PARCEIROS -->

		<div class="container">
			<div id="carrosselLogos" class="owl-Carousel">
				<?php 
					//LOOP DE POST PARCEIROS
					$posParceiros = new WP_Query( array( 'post_type' => 'parceiros', 'orderby' => 'id', 'order' => 'desc', 'posts_per_page' => -1) );
					while ( $posParceiros->have_posts() ) : $posParceiros->the_post();
						$fotoParceiro = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
						$fotoParceiro = $fotoParceiro[0];
				 ?>
				<!-- ITEM -->
				<figure class="item">
					<!-- LOGO PARCEIRO-->
					<img src="<?php echo $fotoParceiro ?>" alt="<?php echo get_the_title() ?>" class="hvr-push">
				</figure>

				<?php endwhile; wp_reset_query(); ?>

			</div>
		</div>
	</section>

	<!-- SESSÃO NEWSLETTER -->
	<section class="sessaoNewsletter background">

		<!--START Scripts : this is the script part you can add to the header of your theme-->
		<script type="text/javascript" src="http://unikapsicologia.com.br/wp-includes/js/jquery/jquery.js?ver=2.7.14"></script>
		<script type="text/javascript" src="http://unikapsicologia.com.br/wp-content/plugins/wysija-newsletters/js/validate/languages/jquery.validationEngine-pt.js?ver=2.7.14"></script>
		<script type="text/javascript" src="http://unikapsicologia.com.br/wp-content/plugins/wysija-newsletters/js/validate/jquery.validationEngine.js?ver=2.7.14"></script>
		<script type="text/javascript" src="http://unikapsicologia.com.br/wp-content/plugins/wysija-newsletters/js/front-subscribers.js?ver=2.7.14"></script>
		<script type="text/javascript">
			/* <![CDATA[ */
			var wysijaAJAX = {"action":"wysija_ajax","controller":"subscribers","ajaxurl":"http://unikapsicologia.com.br/wp-admin/admin-ajax.php","loadingTrans":"Carregando..."};
			/* ]]> */
		</script><script type="text/javascript" src="http://unikapsicologia.com.br/wp-content/plugins/wysija-newsletters/js/front-subscribers.js?ver=2.7.14"></script>
		<!--END Scripts-->

		<div class="gradeFundo">
			<div class="container">
				<h6><?php echo $configuracao['pg_inicial_new_titulo'] ?></h6>

				<div class="row">
					<div class="col-md-5">
						<p><?php echo $configuracao['pg_inicial_new_texto'] ?></p>
					</div>
					<div class="col-md-6">
						<div class="widget_wysija_cont html_wysija">
							
							<div class="widget_wysija_cont html_wysija"><div id="msg-form-wysija-html59f89b3ce731e-2" class="wysija-msg ajax"></div>
							<form id="form-wysija-html59f89b3ce731e-2" method="post" action="#wysija" class="widget_wysija html_wysija">

								<div class="form">
									<div class="row">
										<div class="col-xs-8">

											<label class="hidden">Email <span class="wysija-required">*</span></label>

											<input type="text" name="wysija[user][email]" class="wysija-input validate[required,custom[email]]" title="E-mail" placeholder="E-mail" value="" />

											<span class="abs-req">
												<input type="text" name="wysija[user][abs][email]" class="wysija-input validated[abs][email]" value="" />
											</span>
										</div>
										<div class="col-xs-4">
											<input class="wysija-submit-field" type="submit" value="Enviar" />
											<input type="hidden" name="form_id" value="2" />
											<input type="hidden" name="action" value="save" />
											<input type="hidden" name="controller" value="subscribers" />
											<input type="hidden" value="1" name="wysija-page" />
											<input type="hidden" name="wysija[user_list][list_ids]" value="1" />
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<?php get_footer(); ?>