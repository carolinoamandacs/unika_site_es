<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Unika
 */
global $configuracao;
?>

<!-- RODAPÉ -->
<footer class="rodape">
	<div class="container correcaoX">
		<div class="row">
			<div class="col-sm-2">
				<div class="mapaSite">
					<span>WEBSITE</span>
					<?php 
						$menu = array(
							'theme_location'  => '',
							'menu'            => 'Menu Principal',
							'container'       => false,
							'container_class' => '',
							'container_id'    => '',
							'menu_class'      => 'nav navbar-nav',
							'menu_id'         => '',
							'echo'            => true,
							'fallback_cb'     => 'wp_page_menu',
							'before'          => '',
							'after'           => '',
							'link_before'     => '',
							'link_after'      => '',
							'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
							'depth'           => 2,
							'walker'          => ''
							);
						wp_nav_menu( $menu );
					?>
				</div>
			</div>
			<div class="col-sm-2">
				<div class="mapaSite">
					<span>SOCIAL</span>
					<!-- <a href="<?php echo $configuracao['opt_face'] ?>" target="_blank" title="Facebook">
							Facebook
						</a> -->
					<a href="<?php echo $configuracao['opt_linkedin'] ?>" target="_blank" title="Linkedin">
							Linkedin
						</a> 
						<!-- <a href="<?php echo $configuracao['opt_instagram'] ?>" target="_blank" title="Instagram">
							Instagram
						</a> -->
				</div>
			</div>
			<div class="col-sm-4">
				<div class="mapaSite">
					<span>CONTACTO</span>
					<?php 
						$endereco  = explode("|", $configuracao['opt_endereco']);
					?>
						<a  href="malito:<?php echo$configuracao['opt_Email'] ?>"><?php echo $configuracao['opt_Email'] ?></a>
						<a  href="tel:<?php echo$configuracao['opt_telefone2'] ?>"><?php echo $configuracao['opt_telefone2'] ?></a>
						<a target="_blank"  href="https://www.google.com.br/maps/place/<?php echo $endereco[0] ?>,<?php echo $endereco[1] ?>"><?php echo $endereco[0] ?></a>
						<a target="_blank" href="https://www.google.com.br/maps/place/<?php echo $endereco[0] ?>,<?php echo $endereco[1] ?>"><?php echo $endereco[1] ?></a>			
					</div>
			</div>
			<div class="col-sm-4">
				<span>ENTRE EN CONTACTO</span>
				<br>
				<br>
		
				<div class="formularioContato">
					<?php echo do_shortcode('[contact-form-7 id="4" title="Formulário de contato rodapé"]'); ?>
				</div>
			</div>
		</div>
		<div class="copyright">
			<div class="row">
				<div class="col-sm-6">
					<p><?php echo $configuracao['opt_Copyryght'] ?></p>
				</div>
				<div class="col-sm-6 text-right">
					<a href="http://www.handgran.com/" target="_blank">
						<img src="<?php bloginfo('template_directory'); ?>/img/logohandgran.png" alt="handgran" class="">
					</a>
				</div>
			</div>
			<div class="text-center">
				<img src="<?php echo $configuracao['opt_logoRodape']['url'] ?>" alt="Unika" class="">
			</div>
		</div>
	</div>
</footer>

<?php wp_footer(); ?>

</body>
</html>
