<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Unika
 */
$fotoPost = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
$fotoPost = $fotoPost[0];
$categoriadeAtual = get_the_category();
global $post;


$relacionados_post = rwmb_meta('Unika_relacionados_post');
get_header(); ?>
<!-- POST -->
<div class="pg pg-blogPost">
	<div class="banner">
		<figure style="background: url()">
			<img src="<?php echo $fotoPost ?>" alt="">
		</figure>
	</div>
	
	<div class="container">

		<article id="conteudo">
			<span><?php the_time('F j, Y '); ?></span>
			<h2><?php echo get_the_title() ?></h2>

			<?php echo the_content() ?>
		</article>

	</div>

	<div class="container">	
		<div class="autor">
			<span class="publicacao">Publicado Por</span>
			<div class="row">
				<div class="col-sm-2 col-xs-5">
					<?php $user = wp_get_current_user(); if($user): ?>
					<figure><?php echo get_avatar(get_the_author_meta('ID'), 96); ?> </figure>
					<?php   endif; ?>
					
				</div>
				<div class="col-sm-10 col-xs-7">
					<div class="perfil">
						<h2><?php echo $author = get_the_author(); ?></h2>
						<span>Bio</span>
						<p style="    max-width: 167px;display: inline-block;vertical-align: top;margin-left: 25px;"><?php echo the_author_meta('description') ?></p>

					</div>
				</div>
			</div>
		</div>
	</div>


	<section class="sessaoBlog">	
		<div class="container">
			<h6>Você pode também se interessar por…</h6>
			<div class="posts">
				<div class="row">
					<?php 
					$relacionados = new WP_Query( array( 'post_type' => 'post', 'orderby' => 'id','posts_per_page' => -1) );
					//LOOP DE POST DESTAQUES
					while ( $relacionados->have_posts() ) : $relacionados->the_post();
						$fotoPost = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
						$fotoPost = $fotoPost[0];
						global $post;
						$verificacao =  in_array($post->ID,$relacionados_post);

						$categoriadePost = get_the_category();
						if ($verificacao):
							?>
							<div class="col-md-4">
								<a  href=" <?php echo get_permalink() ?>/#conteudo " class="post">
									<figure style="background: url( <?php echo $fotoPost ?>);"></figure>
									<h2><?php echo get_the_title() ?></h2>
									<span><?php the_time('F j, Y '); ?></span>
									<p><?php customExcerpt(170); ?></p>
								</a>
							</div>
						<?php endif;endwhile; wp_reset_query(); ?>
					</div>
				</div>
			</div>
	</section>

	<!-- SESSÃO NEWSLETTER -->
	<section class="sessaoNewsletter background">

		<!--START Scripts : this is the script part you can add to the header of your theme-->
		<script type="text/javascript" src="http://localhost/projetos/unika/wp-includes/js/jquery/jquery.js?ver=2.7.14"></script>
		<script type="text/javascript" src="http://localhost/projetos/unika/wp-content/plugins/wysija-newsletters/js/validate/languages/jquery.validationEngine-pt.js?ver=2.7.14"></script>
		<script type="text/javascript" src="http://localhost/projetos/unika/wp-content/plugins/wysija-newsletters/js/validate/jquery.validationEngine.js?ver=2.7.14"></script>
		<script type="text/javascript" src="http://localhost/projetos/unika/wp-content/plugins/wysija-newsletters/js/front-subscribers.js?ver=2.7.14"></script>
		<script type="text/javascript">
			/* <![CDATA[ */
			var wysijaAJAX = {"action":"wysija_ajax","controller":"subscribers","ajaxurl":"http://localhost/projetos/unika/wp-admin/admin-ajax.php","loadingTrans":"Carregando..."};
			/* ]]> */
		</script><script type="text/javascript" src="http://localhost/projetos/unika/wp-content/plugins/wysija-newsletters/js/front-subscribers.js?ver=2.7.14"></script>
		<!--END Scripts-->

		<div class="gradeFundo">
			<div class="container">
				<h6><?php echo $configuracao['pg_inicial_new_titulo'] ?></h6>

				<div class="row">
					<div class="col-md-5">
						<p><?php echo $configuracao['pg_inicial_new_texto'] ?></p>
					</div>
					<div class="col-md-6">
						<div class="widget_wysija_cont html_wysija">
							
							<div class="widget_wysija_cont html_wysija"><div id="msg-form-wysija-html59f89b3ce731e-2" class="wysija-msg ajax"></div>
							<form id="form-wysija-html59f89b3ce731e-2" method="post" action="#wysija" class="widget_wysija html_wysija">

								<div class="form">
									<div class="row">
										<div class="col-xs-8">

											<label class="hidden">Email <span class="wysija-required">*</span></label>

											<input type="text" name="wysija[user][email]" class="wysija-input validate[required,custom[email]]" title="your email..." placeholder="your email..." value="" />

											<span class="abs-req">
												<input type="text" name="wysija[user][abs][email]" class="wysija-input validated[abs][email]" value="" />
											</span>
										</div>
										<div class="col-xs-4">
											<input class="wysija-submit-field" type="submit" value="Enviar" />
											<input type="hidden" name="form_id" value="2" />
											<input type="hidden" name="action" value="save" />
											<input type="hidden" name="controller" value="subscribers" />
											<input type="hidden" value="1" name="wysija-page" />
											<input type="hidden" name="wysija[user_list][list_ids]" value="1" />
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
	
<?php
get_footer();
