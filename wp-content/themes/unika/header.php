<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Unika
 */
global $configuracao;
?>
<!doctype html>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="profile" href="http://gmpg.org/xfn/11">
		<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/img/favicon.ico" type="image/x-icon"/>


		<?php wp_head(); ?>

		<!-- Google Tag Manager -->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-55ZWXQG');</script>
		<!-- End Google Tag Manager -->
	</head>
	
	
	<body <?php body_class(); ?>>


	<!-- Google Tag Manager (noscript) -->
	<!-- <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-55ZWXQG"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript> -->
	<!-- End Google Tag Manager (noscript) -->

	

<!-- TOPO -->
<header class="topo">
	
		
		<div class="row">

			<!-- LOGO -->
			<div class="col-md-3 col-sm-3">
				<div class="logo">
					<a href="<?php echo home_url('/'); ?>">
						<img src="<?php echo $configuracao['opt_logo']['url'] ?>" alt="Unika" class="img-responsive">
					</a>
				</div>
			</div>

			<!-- MENU -->
			<div class="col-md-6 col-sm-9">
				<div class="navbar" role="navigation">	

					<!-- MENU MOBILE TRIGGER -->
					<button type="button" id="botao-menu" class="navbar-toggle collapsed hvr-pop" data-toggle="collapse" data-target="#collapse">
						<span class="sr-only"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>

					<!--  MENU MOBILE-->
					<div class="row navbar-header">			
						<nav class="collapse navbar-collapse" id="collapse">
							<?php 
								$menu = array(
									'theme_location'  => '',
									'menu'            => 'Menu Principal',
									'container'       => false,
									'container_class' => '',
									'container_id'    => '',
									'menu_class'      => 'nav navbar-nav',
									'menu_id'         => '',
									'echo'            => true,
									'fallback_cb'     => 'wp_page_menu',
									'before'          => '',
									'after'           => '',
									'link_before'     => '',
									'link_after'      => '',
									'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
									'depth'           => 2,
									'walker'          => ''
									);
								wp_nav_menu( $menu );
							?>
						</nav>						
					</div>			
				</div>
			</div>

			<!-- REDES SOCIAIS -->
			<div class="col-md-3">
				<!-- <div class="idioma">
					<p class="selecaobtn">
						<img src="<?php bloginfo('template_directory'); ?>/img/bandeira.png" alt="" class="imdSelecionado">
						<i class="fa fa-caret-down" aria-hidden="true"></i>
					</p>

					<div class="idiomasSelect">
						<div class="selects">						
							<span class="lBR">
								<img src="<?php bloginfo('template_directory'); ?>/img/bandeira.png" alt=""> Português
							</span>
						</div>

						<div class="selects">
							<span class="lES">
								<img src="<?php bloginfo('template_directory'); ?>/img/bandeira2.png"" alt=""> Espanhol
							</span>
						</div>

					</div>
				</div> -->
				<div class="row">

					<div class="col-sm-6">	
						<div class="redesSociais">
							<a  style="display: none" href="<?php echo $configuracao['opt_face'] ?>" target="_blank" title="Facebook">
								<i class="fa fa-facebook" aria-hidden="true"></i>
							</a>
							<a href="<?php echo $configuracao['opt_linkedin'] ?>" target="_blank" title="Linkedin">
								<i class="fa fa-linkedin" aria-hidden="true"></i>
							</a>
							<a  style="display: none" href="<?php echo $configuracao['opt_instagram'] ?>" target="_blank" title="Instagram">
								<i class="fa fa-instagram" aria-hidden="true"></i>
							</a>
						</div>
					</div>
					<div class="col-sm-6">				
						<div class="indiomas">
							<a href="http://unikapsicologia.com.br/">Português |</a>
							<a href="http://unikapsicologia.com.br/es/">Espanhol</a>
						</div>
					</div>
				</div>
			</div>

		</div>
	
</header>