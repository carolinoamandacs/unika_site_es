<?php
/**
 * Template Name: Problemas que resolvemos
 * Description: Problemas que resolvemos
 *
 * @package Unika
 */

get_header(); ?>
<!-- PÁGINA QUEM SOMOS -->
<div class="pg pg-quemSomos">
	<div class="topoPag"></div>
	

	<!-- SESSÃO DE SERVIÇOS -->
	<section class="listasessaoServicos background">
		<h6 id="problemas-que-resolvemos"><?php echo $configuracao['pg_inicial_problemas_titulo'] ?></h6>
			<p><?php echo $configuracao['pg_inicial_problemas_texto'] ?></p>
		<div class="container">
			
			<ul>
				<?php 
					//LOOP DE POST SERVIÇOS
					$posServicos = new WP_Query( array( 'post_type' => 'servico', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1) );
					while ( $posServicos->have_posts() ) : $posServicos->the_post();
					$fotoServicos = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
					$fotoServicos = $fotoServicos[0];
					
				 ?>
				<!-- SERVIÇOS -->
				<li>
					<div class="servico">
						<img src="<?php echo $fotoServicos ?>" alt="<?php echo get_the_title() ?>">
						<p><?php echo get_the_content() ?></p>
					</div>
				</li>
				<?php endwhile; wp_reset_query(); ?>

			</ul>

		</div>

	</section>

	<!-- SESSÃO ONDE ATUAMOS -->
	<section class="sessaoOndeAtuamos background">
		
		<div class="container correcaoPX">
			<div class="row">
				<div class="col-md-6">
					<div class="titulo">
						<h6><?php echo $configuracao['pg_inicial_onde_titulo'] ?></h6>
						<p><?php echo $configuracao['pg_inicial_onde_texto'] ?></p>

						<a href="<?php echo home_url('/contato/'); ?>">Entre en contacto</a>
					</div>
				</div>
				<div class="col-md-6">
					<div class="listaPaises" style="display: none;">
						<ul>
							<?php 
								//LOOP DE POST DESTAQUES
								$posPaises = new WP_Query( array( 'post_type' => 'paises', 'orderby' => 'id', 'order' => 'desc', 'posts_per_page' => -1) );
								while ( $posPaises->have_posts() ) : $posPaises->the_post();
							 ?>
							<li>
								<span><?php echo get_the_title() ?></span>
							</li>
							<?php endwhile; wp_reset_query(); ?>
						</ul>
					</div>
				</div>
			</div>

			<style>	
				.popUp-News #btnCadastrarNews{
					border: none!important;
				}
		</style>

			<div class="paises">
				<ul>
					<?php 
						//LOOP DE POST DESTAQUES
						$posPaises = new WP_Query( array( 'post_type' => 'paises', 'orderby' => 'id', 'order' => 'desc', 'posts_per_page' => -1) );
						while ( $posPaises->have_posts() ) : $posPaises->the_post();
						$fotoPaises = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
						$fotoPaises = $fotoPaises[0];
						
					?>
					<li>
						<img src="<?php echo $fotoPaises  ?>" alt="<?php echo get_the_title() ?>">
						<h2><?php echo get_the_title() ?></h2>
						<p><?php echo get_the_content() ?></p>
					</li>
					<?php endwhile; wp_reset_query(); ?>
				</ul>
			</div>
		</div>

	</section>

	<!-- SESSÃO  LOGOS PARCEIROS -->
	<section class="sessaoParceiros background">
		<h6 class="hidden">Parceiros</h6>
		<!-- CARROSSEL DE PARCEIROS -->

		<div class="container">
			<div id="carrosselLogos" class="owl-Carousel">
				<?php 
					//LOOP DE POST PARCEIROS
					$posParceiros = new WP_Query( array( 'post_type' => 'parceiros', 'orderby' => 'id', 'order' => 'desc', 'posts_per_page' => -1) );
					while ( $posParceiros->have_posts() ) : $posParceiros->the_post();
						$fotoParceiro = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
						$fotoParceiro = $fotoParceiro[0];
				 ?>
				<!-- ITEM -->
				<figure class="item">
					<!-- LOGO PARCEIRO-->
					<img src="<?php echo $fotoParceiro ?>" alt="<?php echo get_the_title() ?>" class="hvr-push">
				</figure>

				<?php endwhile; wp_reset_query(); ?>

			</div>
		</div>
	</section>

	<!-- SESSÃO NEWSLETTER -->
	<section class="sessaoNewsletter background">

		<!--START Scripts : this is the script part you can add to the header of your theme-->
		<script type="text/javascript" src="http://unikapsicologia.com.br/es/wp-includes/js/jquery/jquery.js?ver=2.7.14"></script>
		<script type="text/javascript" src="http://unikapsicologia.com.br/es/wp-content/plugins/wysija-newsletters/js/validate/languages/jquery.validationEngine-pt.js?ver=2.7.14"></script>
		<script type="text/javascript" src="http://unikapsicologia.com.br/es/wp-content/plugins/wysija-newsletters/js/validate/jquery.validationEngine.js?ver=2.7.14"></script>
		<script type="text/javascript" src="http://unikapsicologia.com.br/es/wp-content/plugins/wysija-newsletters/js/front-subscribers.js?ver=2.7.14"></script>
		<script type="text/javascript">
			/* <![CDATA[ */
			var wysijaAJAX = {"action":"wysija_ajax","controller":"subscribers","ajaxurl":"http://unikapsicologia.com.br/es/wp-admin/admin-ajax.php","loadingTrans":"Carregando..."};
			/* ]]> */
		</script><script type="text/javascript" src="http://unikapsicologia.com.br/es/wp-content/plugins/wysija-newsletters/js/front-subscribers.js?ver=2.7.14"></script>
		<!--END Scripts-->

		<div class="gradeFundo">
			<div class="container">
				<h6><?php echo $configuracao['pg_inicial_new_titulo'] ?></h6>

				<div class="row">
					<div class="col-md-5">
						<p><?php echo $configuracao['pg_inicial_new_texto'] ?></p>
					</div>
					<div class="col-md-6">
						<div class="widget_wysija_cont html_wysija">
							
							<div class="widget_wysija_cont html_wysija"><div id="msg-form-wysija-html59f89b3ce731e-2" class="wysija-msg ajax"></div>
							<form id="form-wysija-html59f89b3ce731e-2" method="post" action="#wysija" class="widget_wysija html_wysija">

								<div class="form">
									<div class="row">
										<div class="col-xs-8">

											<label class="hidden">Email <span class="wysija-required">*</span></label>

											<input type="text" name="wysija[user][email]" class="wysija-input validate[required,custom[email]]" title="E-mail" placeholder="E-mail" value="" />

											<span class="abs-req">
												<input type="text" name="wysija[user][abs][email]" class="wysija-input validated[abs][email]" value="" />
											</span>
										</div>
										<div class="col-xs-4">
											<input class="wysija-submit-field" type="submit" value="Enviar" />
											<input type="hidden" name="form_id" value="2" />
											<input type="hidden" name="action" value="save" />
											<input type="hidden" name="controller" value="subscribers" />
											<input type="hidden" value="1" name="wysija-page" />
											<input type="hidden" name="wysija[user_list][list_ids]" value="1" />
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<?php get_footer(); ?>