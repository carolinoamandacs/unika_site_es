<?php
/**
 * Template Name: Ferramenta de Segurança Corporal
 * Description: Ferramenta de Segurança Corporal
 *
 * @package Unika
 */
get_header(); ?>
<div class="voltarLista" style="background:<?php echo $configuracao['cor_FSC'] ?>">
	<a href="<?php echo home_url('/soluciones/'); ?>" style="color:<?php echo $configuracao['cor_texto_FSC'] ?>">Volver a la lista</a>
</div>
<!-- PÁGINA DE SERVIÇO -->
<div class="pg pg-servico background">

	<!-- SESSÃO DE SERVIÇOS -->
	<section class="sessaoServico">
		<h6 class="hidden">Serviço</h6>
		
		<div class="servico" style="background:<?php echo $configuracao['cor_FSC'] ?>">
				<div class="container">
					<div class="row">
						<div class="col-sm-2">
							<figure style="	background-color: rgba(0,0,0,0.1);">
								<img src="<?php echo $configuracao['icone_FSC']['url'] ?>" alt="<?php echo $configuracao['titulo_FSC'] ?>">
							</figure>
						</div>
						<div class="col-sm-10">
							<h2 style="color:<?php echo $configuracao['cor_texto_FSC'] ?>"><?php echo $configuracao['titulo_FSC'] ?></h2>
							<p style="color:<?php //echo $configuracao['cor_texto_FSC'] ?>"><?php //echo $configuracao['subititulo_FSC'] ?> </p>
						</div>
					</div>
				</div>
		</div>
		
		<div class="containerLargura correcaoX areasServicos">
			
			<article class="descricaoLayoutB">
				<div class="row">
					<div class="col-sm-12">
						<div class="comofazemos">
							<strong><?php echo $configuracao['quadros_titulo_esquerdo_FSC'] ?></strong>
							<p><?php echo $configuracao['quadros_conteudo_esquerdo_FSC'] ?> </p>
						</div>
					</div>
				</div>
			</article>

			<article class="listasdeServicos">
				<div class="conteudo">
					<div class="row">
						<div class="col-sm-12">
							<div class="servicosPossiveis">
								<span>Servicios posibles</span>
								<?php foreach ($configuracao['quadros_servicos_esquerdo_FSC'] as $configuracao['quadros_servicos_esquerdo_FSC']):
								?>	
								<p><?php echo $configuracao['quadros_servicos_esquerdo_FSC'] ?></p>
							<?php endforeach; ?>

								
								<div>
									<a href="http://unikapsicologia.com.br/es/palabra-llave-dds-dialogos-diarios-de-seguridad-en-el-trabajo-comunicacion-eficaz-en-seguridad-temas-para-dds/#conteudo">Más información</a>
								</div> 
								
							</div>
						</div>
					</div>
				</div>
			</article>

			<article class="descricaoLayoutB">
				<div class="row">
					<div class="col-sm-12">
						<div class="comofazemos">
							<strong><?php echo $configuracao['quadros_titulo_direito_FSC'] ?></strong>
							<p><?php echo $configuracao['quadros_conteudo_direito_FSC'] ?> </p>
						</div>
					</div>
				</div>
			</article>


			<article class="listasdeServicos">
				<div class="conteudo">
					<div class="row">
						<div class="col-sm-12">
							<div class="servicosPossiveis">
								<span>Servicios posibles</span>
								<?php foreach ($configuracao['quadros_servicos_direito_FSC'] as $configuracao['quadros_servicos_direito_FSC']):
								?>	
								<p><?php echo $configuracao['quadros_servicos_direito_FSC'] ?></p>
							<?php endforeach; ?>

								<div>
									<a href="http://unikapsicologia.com.br/es/palabras-llave-oac-observacion-y-abordaje-de-comportamientos-en-seguridad/#conteudo">Más información</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</article>

		</div>

		<div class="containerLargura correcaoX areasServicos">
			
			<article class="descricaoLayoutB">
				<div class="row">
					<div class="col-sm-12">
						<div class="comofazemos">
							<strong><?php echo $configuracao['quadros_titulo_esquerdo_Segundo_FSC'] ?></strong>
							<p><?php echo $configuracao['quadros_conteudo_esquerdo_Segundo_FSC'] ?> </p>
						</div>
					</div>
				</div>
			</article>

			<article class="listasdeServicos">
				<div class="conteudo">
					<div class="row">
						<div class="col-sm-12">
							<div class="servicosPossiveis">
								<span>Servicios posibles</span>
								<?php foreach ($configuracao['quadros_servicos_esquerdo_Segundo_FSC'] as $configuracao['quadros_servicos_esquerdo_Segundo_FSC']):
								?>	
								<p><?php echo $configuracao['quadros_servicos_esquerdo_Segundo_FSC'] ?></p>
								<?php endforeach; ?>
								<!-- <div>
									<a href="#">Más información</a>
								</div> -->
							</div>
						</div>
					</div>
				</div>

			</article>
		

			<article class="descricaoLayoutB">
				<div class="row">
					<div class="col-sm-12">
						<div class="comofazemos">
							<strong><?php echo $configuracao['quadros_titulo_direito_Segundo_FSC'] ?></strong>
							<p><?php echo $configuracao['quadros_conteudo_direito_Segundo_FSC'] ?> </p>
						</div>
					</div>
				</div>
			</article>


			<article class="listasdeServicos">
				<div class="conteudo">
					<div class="row">
						<div class="col-sm-12">
							<div class="servicosPossiveis">
								<span>Servicios posibles</span>
								<?php foreach ($configuracao['quadros_servicos_direito_Segundo_FSC'] as $configuracao['quadros_servicos_direito_Segundo_FSC']):
								?>	
								<p><?php echo $configuracao['quadros_servicos_direito_Segundo_FSC'] ?></p>
								<?php endforeach; ?>
								<!-- <div>
									<a href="#">Más información</a>
								</div> -->
							</div>
						</div>
					</div>
				</div>

			</article>

		</div>

		<div class="containerLargura correcaoX areasServicos">
			
			<article class="descricaoLayoutB">
				<div class="row">
					<div class="col-sm-12">
						<div class="comofazemos">
							<strong><?php echo $configuracao['quadros_titulo_esquerdo_Tegundo_FSC'] ?></strong>
							<p><?php echo $configuracao['quadros_conteudo_esquerdo_Tegundo_FSC'] ?> </p>
						</div>
					</div>
					
				</div>
			</article>

			<article class="listasdeServicos">
				<div class="conteudo">
					<div class="row">
						<div class="col-sm-12">
							<div class="servicosPossiveis">
								<span>Servicios posibles</span>
								<?php foreach ($configuracao['quadros_servicos_esquerdo_Tegundo_FSC'] as $configuracao['quadros_servicos_esquerdo_Tegundo_FSC']):
								?>	
								<p><?php echo $configuracao['quadros_servicos_esquerdo_Tegundo_FSC'] ?></p>
								<?php endforeach; ?>
								<!-- <div>
									<a href="#">Más información</a>
								</div> -->
							</div>
						</div>
					</div>
				</div>

			</article>


			<article class="descricaoLayoutB">
				<div class="row">
					
					<div class="col-sm-12">
						<div class="comofazemos">
							<strong><?php echo $configuracao['quadros_titulo_direito_Tegundo_FSC'] ?></strong>
							<p><?php echo $configuracao['quadros_conteudo_direito_Tegundo_FSC'] ?> </p>
						</div>
					</div>
				</div>
			</article>

			<article class="listasdeServicos">
				<div class="conteudo">
					<div class="row">
						<div class="col-sm-12">
							<div class="servicosPossiveis">
								<span>Servicios posibles</span>
								<?php foreach ($configuracao['quadros_servicos_direito_Tegundo_FSC'] as $configuracao['quadros_servicos_direito_Tegundo_FSC']):
								?>	
								<p><?php echo $configuracao['quadros_servicos_direito_Tegundo_FSC'] ?></p>
								<?php endforeach; ?>
								<!-- <div>
									<a href="#">Más información</a>
								</div> -->
							</div>
						</div>
					</div>
				</div>

			</article>

		</div>

		<div class="containerLargura correcaoX areasServicos">
			
			<article class="descricaoLayoutB">
				<div class="row">
					<div class="col-sm-12">
						<div class="comofazemos">
							<strong><?php echo $configuracao['quadros_titulo_esquerdo_quarto_FSC'] ?></strong>
							<p><?php echo $configuracao['quadros_conteudo_esquerdo_quarto_FSC'] ?> </p>
						</div>
					</div>
				</div>
			</article>


			<article class="listasdeServicos">
				<div class="conteudo">
					<div class="row">
						<div class="col-sm-12">
							<div class="servicosPossiveis">
								<span>Servicios posibles</span>
								<?php foreach ($configuracao['quadros_servicos_esquerdo_quarto_FSC'] as $configuracao['quadros_servicos_esquerdo_quarto_FSC']):
								?>	
								<p><?php echo $configuracao['quadros_servicos_esquerdo_quarto_FSC'] ?></p>
								<?php endforeach; ?>
								<!-- <div>
									<a href="#">Más información</a>
								</div> -->
							</div>
						</div>
					</div>
				</div>

			</article>

			<article class="descricaoLayoutB">
				<div class="row">
					<div class="col-sm-12">
						<div class="comofazemos">
							<strong><?php echo $configuracao['quadros_titulo_direito_quarto_FSC'] ?></strong>
							<p><?php echo $configuracao['quadros_conteudo_direito_quarto_FSC'] ?> </p>
						</div>
					</div>
				</div>
			</article>

			<article class="listasdeServicos">
				<div class="conteudo">
					<div class="row">
						
						<div class="col-sm-12">
							<div class="servicosPossiveis">
								<span>Servicios posibles</span>
								<?php foreach ($configuracao['quadros_servicos_direito_quarto_FSC'] as $configuracao['quadros_servicos_direito_quarto_FSC']):
								?>	
								<p><?php echo $configuracao['quadros_servicos_direito_quarto_FSC'] ?></p>
								<?php endforeach; ?>
								<div>
									<a href="http://unikapsicologia.com.br/es/palabras-llave-indicadores-preventivos-indicadores-de-seguridad-indicadores-de-liderazgo/#conteudo">Más información</a>
								</div>
							</div>
						</div>
					</div>
				</div>

			</article>

		</div>

		<div class="containerLargura correcaoX areasServicos">
			
			<article class="descricaoLayoutB">
				<div class="row">
					<div class="col-sm-12">
						<div class="comofazemos">
							<strong><?php echo $configuracao['quadros_titulo_esquerdo_quinto_FSC'] ?></strong>
							<p><?php echo $configuracao['quadros_conteudo_esquerdo_quinto_FSC'] ?> </p>
						</div>
					</div>
					
				</div>
			</article>

			<article class="listasdeServicos">
				<div class="conteudo">
					<div class="row">
						<div class="col-sm-12">
							<div class="servicosPossiveis">
								<span>Servicios posibles</span>
								<?php foreach ($configuracao['quadros_servicos_esquerdo_quinto_FSC'] as $configuracao['quadros_servicos_esquerdo_quinto_FSC']):
								?>	
								<p><?php echo $configuracao['quadros_servicos_esquerdo_quinto_FSC'] ?></p>
								<?php endforeach; ?>
								
								<div>
									<a href="http://unikapsicologia.com.br/es/comportamiento-humano-que-necesita-saber-para-evitar-accidentes-del-trabajo/#conteudo">Más información</a>
								</div>
							</div>
						</div>
						
					</div>
				</div>

			</article>

			<article class="descricaoLayoutB">
				<div class="row">
					
					<div class="col-sm-12">
						<div class="comofazemos">
							<strong><?php echo $configuracao['quadros_titulo_direito_quinto_FSC'] ?></strong>
							<p><?php echo $configuracao['quadros_conteudo_direito_quinto_FSC'] ?> </p>
						</div>
					</div>
				</div>
			</article>


			<article class="listasdeServicos">
				<div class="conteudo">
					<div class="row">
						
						<div class="col-sm-12">
							<div class="servicosPossiveis">
								<span>Servicios posibles</span>
								<?php foreach ($configuracao['quadros_servicos_direito_quinto_FSC'] as $configuracao['quadros_servicos_direito_quinto_FSC']):
								?>	
								<p><?php echo $configuracao['quadros_servicos_direito_quinto_FSC'] ?></p>
								<?php endforeach; ?>
								
							</div>
						</div>
					</div>
				</div>

			</article>

		</div>

		<!-- SESSÃO NEWSLETTER -->
		<section class="sessaoNewsletter background">

			<!--START Scripts : this is the script part you can add to the header of your theme-->
			<script type="text/javascript" src="http://unikapsicologia.com.br/wp-includes/js/jquery/jquery.js?ver=2.7.14"></script>
			<script type="text/javascript" src="http://unikapsicologia.com.br/wp-content/plugins/wysija-newsletters/js/validate/languages/jquery.validationEngine-pt.js?ver=2.7.14"></script>
			<script type="text/javascript" src="http://unikapsicologia.com.br/wp-content/plugins/wysija-newsletters/js/validate/jquery.validationEngine.js?ver=2.7.14"></script>
			<script type="text/javascript" src="http://unikapsicologia.com.br/wp-content/plugins/wysija-newsletters/js/front-subscribers.js?ver=2.7.14"></script>
			<script type="text/javascript">
				/* <![CDATA[ */
				var wysijaAJAX = {"action":"wysija_ajax","controller":"subscribers","ajaxurl":"http://unikapsicologia.com.br/wp-admin/admin-ajax.php","loadingTrans":"Carregando..."};
				/* ]]> */
			</script><script type="text/javascript" src="http://unikapsicologia.com.br/wp-content/plugins/wysija-newsletters/js/front-subscribers.js?ver=2.7.14"></script>
			<!--END Scripts-->

			<div class="gradeFundo">
				<div class="container">
					<h6><?php echo $configuracao['pg_inicial_new_titulo'] ?></h6>

					<div class="row">
						<div class="col-md-5">
							<p><?php echo $configuracao['pg_inicial_new_texto'] ?></p>
						</div>
						<div class="col-md-6">
							<div class="widget_wysija_cont html_wysija">
								
								<div class="widget_wysija_cont html_wysija"><div id="msg-form-wysija-html59f89b3ce731e-2" class="wysija-msg ajax"></div>
								<form id="form-wysija-html59f89b3ce731e-2" method="post" action="#wysija" class="widget_wysija html_wysija">

									<div class="form">
										<div class="row">
											<div class="col-xs-8">

												<label class="hidden">Email <span class="wysija-required">*</span></label>

												<input type="text" name="wysija[user][email]" class="wysija-input validate[required,custom[email]]" title="your email..." placeholder="your email..." value="" />

												<span class="abs-req">
													<input type="text" name="wysija[user][abs][email]" class="wysija-input validated[abs][email]" value="" />
												</span>
											</div>
											<div class="col-xs-4">
												<input class="wysija-submit-field" type="submit" value="Enviar" />
												<input type="hidden" name="form_id" value="2" />
												<input type="hidden" name="action" value="save" />
												<input type="hidden" name="controller" value="subscribers" />
												<input type="hidden" value="1" name="wysija-page" />
												<input type="hidden" name="wysija[user_list][list_ids]" value="1" />
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

	</section>
</div>
<?php get_footer(); ?>