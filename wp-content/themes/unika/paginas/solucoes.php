<?php
/**
 * Template Name: Soluções
 * Description: Soluções
 *
 * @package Unika
 */

get_header(); ?>
<!-- PÁGINA DE SERVIÇOS -->
<div class="pg pg-servicos">

	<!-- SESSÃO DE SERVIÇOS -->
	<section class="sessaoServicos">
		
		<h6 class="hidden">Serviços</h6>
		<?php 
			$i = 0;
			//LOOP DE POST DESTAQUES
			$posDestaques = new WP_Query( array( 'post_type' => 'destaque', 'orderby' => 'id', 'order' => 'desc', 'posts_per_page' => -1) );
			while ( $posDestaques->have_posts() ) : $posDestaques->the_post();
				$fotoDestaque = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
				$fotoDestaque = $fotoDestaque[0];
				$destaque_cor = rwmb_meta('Unika_destaque_cor');
				$destaque_cor_circulo = rwmb_meta('Unika_destaque_cor_circulo');
				$destaque_link = rwmb_meta('Unika_destaque_link');
				$destaque_cor_texto = rwmb_meta('Unika_destaque_cor_texto');
				
		 ?>
		<div class="servico <?php if ($i == 0) {echo "correcaoPadding";} ?>" style="background:<?php echo $destaque_cor ?>">
			<a href=" <?php echo $destaque_link ?> ">
				<div class="container">
					<div class="row">
						<div class="col-sm-2">
							<figure style="	background-color: <?php echo $destaque_cor_circulo ?>">
								<img src="<?php echo $fotoDestaque ?>" alt="<?php echo get_the_title() ?>">
							</figure>
						</div>
						<div class="col-sm-10">
							<h2 style="color: <?php echo $destaque_cor_texto ?>"><?php echo get_the_title() ?></h2>
							<p style="color: <?php echo $destaque_cor_texto ?>"><?php //echo get_the_content() ?></p>
						</div>
					</div>
				</div>
			</a>
		</div>
		<?php  $i++;endwhile; wp_reset_query(); ?>

	</section>

</div>
<?php get_footer(); ?>