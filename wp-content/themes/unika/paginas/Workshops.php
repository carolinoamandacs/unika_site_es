<?php
/**
 * Template Name: Workshops
 * Description: Workshops
 *
 * @package Unika
 */

get_header(); ?>
<!-- PÁGINA DE SERVIÇO -->
<div class="voltarLista" style="background:<?php echo $configuracao['cor_W'] ?>">
	<a href="<?php echo home_url('/soluciones/'); ?>" style="color:<?php echo $configuracao['cor_texto_W'] ?>">Volver a la lista</a>
</div>
<div class="pg pg-servico background">

<!-- SESSÃO DE SERVIÇOS -->
<section class="sessaoServico">
	<h6 class="hidden">Serviço</h6>
	
	<div class="servico" style="background:<?php echo $configuracao['cor_W'] ?>">
			<div class="container">
				<div class="row">
					<div class="col-sm-2">
						<figure style="	background-color: rgba(0,0,0,0.1);">
							<img src="<?php echo $configuracao['icone_W']['url'] ?>" alt="<?php echo $configuracao['titulo_W'] ?>">
						</figure>
					</div>
					<div class="col-sm-10">
						<h2 style="color:<?php echo $configuracao['cor_texto_W'] ?>"><?php echo $configuracao['titulo_W'] ?></h2>
						<p style="color:<?php echo $configuracao['cor_texto_W'] ?>"><?php echo $configuracao['subititulo_W'] ?> </p>
					</div>
				</div>
			</div>
		</div>
	
	
		<!-- SESSÃO DE SERVIÇOS -->
		<section class="sessaoServico">
			
			<div class="container correcaoX areasServicos">
				<article class="descricaoLayoutB">
					<h6  class="tituloServico">¿Como hacemos?</h6>
					<div class="row">
						<div class="col-sm-6">
							<div class="comofazemos">
								<p class="paragrafoPersonalizado"><?php echo $configuracao['quadros_conteudo_esquerdo_W'] ?></p>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="comofazemos">
								<p class="paragrafoPersonalizado"><?php echo $configuracao['quadros_conteudo_direito_W'] ?></p>
							</div>
						</div>
					</div>
				</article>

			</div>

		</section>
		
		<article class="descricaoLayoutA displayInlineWorkshops">
			
			<div class="comofazemos comofazemosB">
				
				<div class="container correcaoX">
					<?php foreach ($configuracao['quadros_conteudo_W'] as $configuracao['quadros_conteudo_W']):
						$texto_link = explode("|", $configuracao['quadros_conteudo_W']);
						?>
						<a <?php if ($texto_link[2]) {?> href=" <?php echo $texto_link[2] ?>" <?php } ?> class="displayInline">
							<h2><?php echo $texto_link[0]   ?></h2>
							<p><?php echo $texto_link[1] ?></p>
							<span>Más información</span>
						</a>
					<?php endforeach; ?>
				</div>
			</div>
		</article>
	<!-- SESSÃO NEWSLETTER -->
	<section class="sessaoNewsletter background">

		<!--START Scripts : this is the script part you can add to the header of your theme-->
		<script type="text/javascript" src="http://unikapsicologia.com.br/es/wp-includes/js/jquery/jquery.js?ver=2.7.14"></script>
		<script type="text/javascript" src="http://unikapsicologia.com.br/es/wp-content/plugins/wysija-newsletters/js/validate/languages/jquery.validationEngine-pt.js?ver=2.7.14"></script>
		<script type="text/javascript" src="http://unikapsicologia.com.br/es/wp-content/plugins/wysija-newsletters/js/validate/jquery.validationEngine.js?ver=2.7.14"></script>
		<script type="text/javascript" src="http://unikapsicologia.com.br/es/wp-content/plugins/wysija-newsletters/js/front-subscribers.js?ver=2.7.14"></script>
		<script type="text/javascript">
			/* <![CDATA[ */
			var wysijaAJAX = {"action":"wysija_ajax","controller":"subscribers","ajaxurl":"http://unikapsicologia.com.br/es/wp-admin/admin-ajax.php","loadingTrans":"Carregando..."};
			/* ]]> */
		</script><script type="text/javascript" src="http://unikapsicologia.com.br/es/wp-content/plugins/wysija-newsletters/js/front-subscribers.js?ver=2.7.14"></script>
		<!--END Scripts-->

		<div class="gradeFundo">
			<div class="container">
				<h6><?php echo $configuracao['pg_inicial_new_titulo'] ?></h6>

				<div class="row">
					<div class="col-md-5">
						<p><?php echo $configuracao['pg_inicial_new_texto'] ?></p>
					</div>
					<div class="col-md-6">
						<div class="widget_wysija_cont html_wysija">
							
							<div class="widget_wysija_cont html_wysija"><div id="msg-form-wysija-html59f89b3ce731e-2" class="wysija-msg ajax"></div>
							<form id="form-wysija-html59f89b3ce731e-2" method="post" action="#wysija" class="widget_wysija html_wysija">

								<div class="form">
									<div class="row">
										<div class="col-xs-8">

											<label class="hidden">Email <span class="wysija-required">*</span></label>

											<input type="text" name="wysija[user][email]" class="wysija-input validate[required,custom[email]]" title="E-mail" placeholder="E-mail" value="" />

											<span class="abs-req">
												<input type="text" name="wysija[user][abs][email]" class="wysija-input validated[abs][email]" value="" />
											</span>
										</div>
										<div class="col-xs-4">
											<input class="wysija-submit-field" type="submit" value="Enviar" />
											<input type="hidden" name="form_id" value="2" />
											<input type="hidden" name="action" value="save" />
											<input type="hidden" name="controller" value="subscribers" />
											<input type="hidden" value="1" name="wysija-page" />
											<input type="hidden" name="wysija[user_list][list_ids]" value="1" />
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

</section>
</div>

<?php get_footer(); ?>